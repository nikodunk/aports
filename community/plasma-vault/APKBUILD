# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-vault
pkgver=5.27.7
pkgrel=1
pkgdesc="Plasma applet and services for creating encrypted vaults"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="(GPL-2.0-only OR GPL-3.0-only) AND (LGPL-2.1-only AND LGPL-3.0-only)"
makedepends="
	extra-cmake-modules
	kactivities-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	libksysguard-dev
	networkmanager-qt-dev
	plasma-framework-dev
	qt5-qtbase-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/plasma-vault.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-vault-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f4c4c933166a52a2e4ce530b87e0ac4c3bfe50696b164a614723c76846db278a05cc5c283efcf980a52bdf41f5eb47ee59410c68697ad122d08d090205aaf2d3  plasma-vault-5.27.7.tar.xz
"
