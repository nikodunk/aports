# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kbackup
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.kbackup"
pkgdesc="An application which lets you back up your data in a simple, user friendly way"
license="GPL-2.0-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libarchive-dev
	qt5-qtbase-dev
	samurai
	shared-mime-info
	"
_repo_url="https://invent.kde.org/utilities/kbackup.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kbackup-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9461178270a691f15e2f9d8a4047a9920b28aabf1e64dea99176f42b153b406a067ed917ebeabc36756e86d3302c0d0c8edfc60ca1a65b792286e69122ae3853  kbackup-23.08.0.tar.xz
"
