# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=py3-gitpython
pkgver=3.1.34
pkgrel=0
pkgdesc="Python3 Git Library"
url="https://github.com/gitpython-developers/GitPython"
arch="noarch"
license="BSD-3-Clause"
depends="
	git
	py3-gitdb2
	py3-typing-extensions
	python3
	"
makedepends="py3-setuptools"
checkdepends="
	py3-pytest
	py3-pytest-cov
	py3-pytest-sugar
	py3-toml
	"
subpackages="$pkgname-pyc"
source="https://github.com/gitpython-developers/GitPython/archive/$pkgver/GitPython-$pkgver.tar.gz"
builddir="$srcdir/GitPython-$pkgver"

build() {
	python3 setup.py build
}

check() {
	# There are more tests but they require a specific git configuration
	pytest test/test_config.py
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}

sha512sums="
254d1b8183314b2ac8455218671e42a46c4d470dffe5058084f6330eb4db8c382d180eb73a62fdead3dd832df451b22aa980c0c2713984ffe39d9dda781124ea  GitPython-3.1.34.tar.gz
"
