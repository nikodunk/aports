# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kleopatra
pkgver=23.08.0
pkgrel=0
arch="all !armhf !s390x"
url="https://www.kde.org/applications/utilities/kleopatra/"
pkgdesc="Certificate Manager and Unified Crypto GUI"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="
	gnupg
	pinentry-qt
	"
makedepends="
	boost-dev
	extra-cmake-modules
	gpgme-dev
	kcmutils-dev
	kcodecs-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kitemmodels-dev
	kmime-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libassuan-dev
	libkleo-dev
	qgpgme
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/kleopatra.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kleopatra-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# kuniqueservicetest requires running dbus
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kuniqueservicetest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8e775dce32b27cb2d76aef8ac17b6bf5636f24754e792dab3c5b243f1e448bd66c1540d540f8018adaeb8c2b9c23f2b96ff0297cc6943fd9e15940f8496d31ad  kleopatra-23.08.0.tar.xz
"
