# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kweather
pkgver=23.08.0
pkgrel=0
pkgdesc="Weather application for Plasma Mobile"
url="https://invent.kde.org/plasma-mobile/kweather"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
license="GPL-2.0-or-later AND CC-BY-4.0"
depends="
	kirigami-addons
	kirigami2
	kquickcharts
	"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kirigami-addons-dev
	kirigami2-dev
	knotifications-dev
	kquickcharts-dev
	kweathercore-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtcharts-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/utilities/kweather.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kweather-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5743c1b011c04f457fa4ced99150f35dc95260de317082826fd6ec102bff7b65ad2f94b2d302cc1127cdaf414d7734ddeacb34620c1ed2e4c978189810531e1d  kweather-23.08.0.tar.xz
"
