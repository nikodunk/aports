# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=ki18n
pkgver=5.109.0
pkgrel=0
pkgdesc="Advanced internationalization framework"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-or-later)"
depends_dev="
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	doxygen
	graphviz
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/ki18n.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/ki18n-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# ki18n-klocalizedstringtest, kcountrytest, kcountrysubdivisiontest and kcatalogtest are broken
	xvfb-run ctest --test-dir build --output-on-failure -E "(ki18n-klocalizedstring|kcountry|kcountrysubdivision|kcatalog)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e0c8c37a31f4c857a3a3a383bf5745407fb2b31127ee76cfd86f5ddd38b55b2eaa2063162e9e72bf3fef07e3582efdb7a01ded084eb40af73ea73e240b7d92a5  ki18n-5.109.0.tar.xz
"
