# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kate
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.kate"
pkgdesc="A multi-document, multi-view text editor"
license="LGPL-2.0-only AND LGPL-2.0-or-later"
depends="kate-common"
makedepends="
	extra-cmake-modules
	kactivities-dev
	kconfig-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	kjobwidgets-dev
	knewstuff-dev
	kparts-dev
	ktexteditor-dev
	kuserfeedback-dev
	kwindowsystem-dev
	kxmlgui-dev
	plasma-framework-dev
	qt5-qtbase-dev
	samurai
	threadweaver-dev
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/utilities/kate.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kate-$pkgver.tar.xz"
subpackages="kate-common:kate_common kwrite:kwrite kwrite-doc:kwrite_doc kwrite-lang:kwrite_lang $pkgname-doc $pkgname-lang"

build() {
	cmake -B build/kate -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_kwrite=FALSE
	cmake --build build/kate
	cmake -B build/kwrite -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_kate=FALSE \
		-DBUILD_addons=FALSE
	cmake --build build/kwrite
}

check() {
	cd build/kate
	# kateapp-session_manager_test and kateapp-filetree_model_test are broken
	# kateapp-sessions_action_test requires OpenGL
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(kateapp-session_manager|kateapp-sessions_action|kateapp-filetree_model)_test" -j1
}

package() {
	DESTDIR="$pkgdir" cmake --install build/kate

	mkdir -p "$pkgdir"/usr/share/doc/$pkgname
	mv "$pkgdir"/usr/share/doc/HTML "$pkgdir"/usr/share/doc/$pkgname/
}

kate_common() {
	amove usr/lib
}

kwrite() {
	depends="kate-common"

	cd "$builddir"
	DESTDIR="$subpkgdir" cmake --install build/kwrite

	mkdir -p "$subpkgdir"/usr/share/doc/$subpkgname
	mv "$subpkgdir"/usr/share/doc/HTML "$subpkgdir"/usr/share/doc/$subpkgname/

	# Man package only exists for Kate
	rm -r "$subpkgdir"/usr/share/man

	rm -r "$subpkgdir"/usr/lib
}

kwrite_doc() {
	pkgdesc="KWrite documentation"
	install_if="docs kwrite=$pkgver-r$pkgrel"

	mkdir -p "$subpkgdir"/usr/share/doc/kwrite
	mv "$subpkgdir"/../kwrite/usr/share/doc/kwrite/HTML \
		"$subpkgdir"/usr/share/doc/kwrite
}

kwrite_lang() {
	pkgdesc="Languages for package KWrite"
	install_if="kwrite=$pkgver-r$pkgrel lang"

	local dir
	for dir in ${langdir:-/usr/share/locale}; do
		mkdir -p "$subpkgdir"/${dir%/*}
		mv "$subpkgdir"/../kwrite/"$dir" "$subpkgdir"/"$dir"
	done
}

sha512sums="
6081286c0c6a12b79b8cbc65ca8d73b8181d33dca1745d0199c567f1d23249da786baea7b0bc3d8dd3153782c6eff660b0e57d4bf8ca9aa8fefb016b19deeddd  kate-23.08.0.tar.xz
"
