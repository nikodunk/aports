# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-argh
pkgver=0.29.3
pkgrel=0
pkgdesc="Python3 argparse wrapper"
url="https://github.com/neithere/argh"
arch="noarch"
license="LGPL-3.0-or-later"
depends="python3"
makedepends="
	py3-gpep517
	py3-flit-core
	"
checkdepends="py3-mock py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/neithere/argh/archive/v$pkgver/py3-argh-$pkgver.tar.gz"
builddir="$srcdir/argh-$pkgver"

replaces="py-argh" # Backwards compatibility
provides="py-argh=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
9e7f75f2d4d0a241772e328830a79ebf8987d4e84a73e0d80c8c9b5dbe5729edf0fd2c262f2c844bf08d6996758f007ec924b3c44cb9d5d19ec51a5888f88d75  py3-argh-0.29.3.tar.gz
"
