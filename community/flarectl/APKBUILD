# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=flarectl
pkgver=0.76.0
pkgrel=1
pkgdesc="CLI application for interacting with a Cloudflare account"
url="https://github.com/cloudflare/cloudflare-go/tree/master/cmd/flarectl"
arch="all"
license="BSD-3-Clause"
makedepends="go"
source="https://github.com/cloudflare/cloudflare-go/archive/refs/tags/v$pkgver/flarectl-$pkgver.tar.gz"
builddir="$srcdir/cloudflare-go-$pkgver/cmd/flarectl"
options="!check chmod-clean net" # no testsuite provided

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOFLAGS="$GOFLAGS -buildvcs=false"

build() {
	go build -o flarectl
}

package() {
	install -Dm755 flarectl -t "$pkgdir"/usr/bin
}

sha512sums="
1ebdd6e25daddf3086de6b7b1ddb862ed24a5525cf72ab3df41d888b4582152c6f253d05d1c882c82fa2304073278d1db0a8af1f8285fd73c181f7f599df92c3  flarectl-0.76.0.tar.gz
"
