# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kopeninghours
pkgver=23.08.0
pkgrel=0
pkgdesc="Library for parsing and evaluating OSM opening hours expressions"
url="https://invent.kde.org/libraries/kopeninghours"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="BSD-3-Clause AND CC0-1.0 AND LGPL-2.0-or-later"
makedepends="
	bison
	doxygen
	extra-cmake-modules
	flex
	graphviz
	kholidays-dev
	ki18n-dev
	qt5-qtbase-dev
	qt5-qttools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/libraries/kopeninghours.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kopeninghours-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# evaluatetest and iterationtest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "(evaluate|iteration)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ddc2c026386383ec8aa0768293bfb8fc68ed67eb9f0cf52a133356d82a5d965dfa7855762a82cd36b726068cf371e38b5d964ebbc668577b62d816fb0febc32a  kopeninghours-23.08.0.tar.xz
"
