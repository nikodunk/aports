# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kio-admin
pkgver=23.08.0
pkgrel=0
pkgdesc="Manage files as administrator using the admin:// KIO protocol"
url="https://invent.kde.org/system/kio-admin"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="BSD-3-Clause AND (GPL-2.0-only OR GPL-3.0-only)"
# zstd is purely used to unpack the source archive
makedepends="
	extra-cmake-modules
	ki18n-dev
	kio-dev
	polkit-qt-1-dev
	qt5-qtbase-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/system/kio-admin.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kio-admin-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
472f915423782d255bd3408fa5828bb2983e9746e911c10e648b8f9bc0187125512f1b458abdf639a33e081a782af5b0ffffa2d535ddac9ce17b1a02382e3ab3  kio-admin-23.08.0.tar.xz
"
