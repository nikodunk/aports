# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=qqc2-breeze-style
pkgver=5.27.7
pkgrel=1
pkgdesc="Breeze inspired QQC2 style"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.plasma-mobile.org/"
license="LicenseRef-KDE-Accepted-LGPL AND LicenseRef-KFQF-Accepted-GPL"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kguiaddons-dev
	kiconthemes-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	qt5-qtx11extras-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/qqc2-breeze-style.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/qqc2-breeze-style-$pkgver.tar.xz"
subpackages="$pkgname-dev"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
aef8d3ee6745a122b3e3a790033f3dac96ed3a15a5634fd27e8828084b17c5a0ae8109294e660610d1f90c0fc0a11a58c6a52e62ebc54b05919f6b4f37a94487  qqc2-breeze-style-5.27.7.tar.xz
"
