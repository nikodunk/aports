# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kdegraphics-thumbnailers
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
pkgdesc="Thumbnailers for various graphics file formats"
url="https://www.kde.org/applications/graphics/"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	karchive-dev
	kdegraphics-mobipocket-dev
	kio-dev
	libkdcraw-dev
	libkexiv2-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/graphics/kdegraphics-thumbnailers.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdegraphics-thumbnailers-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0192d77950319269c933c3bb861c2f9049dce6510c177e8fa8b5ccb5cfc25d87b70a2f8e37f1cddbfb6b9d6e9aa714f20e2a4aceb418fd533a161f7f0cfe5d67  kdegraphics-thumbnailers-23.08.0.tar.xz
"
