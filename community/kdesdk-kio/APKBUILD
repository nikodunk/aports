# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kdesdk-kio
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/development"
pkgdesc="KIO-Slaves"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kio-dev
	perl-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/sdk/kdesdk-kio.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdesdk-kio-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2a8a3096d61028eec0caec4b5d912d2fb1575bc92a1ee9a24cd6fa37fe6b42ce18e583ccffa16b1960370e0b87685a35cf73d6a6f170dd6fc5236dd1fb9fcfa6  kdesdk-kio-23.08.0.tar.xz
"
