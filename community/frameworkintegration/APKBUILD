# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=frameworkintegration
pkgver=5.109.0
pkgrel=0
pkgdesc="Framework providing components to allow applications to integrate with a KDE Workspace"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org"
license="LGPL-2.1-or-later"
depends_dev="
	appstream-dev
	kconfig-dev
	kconfigwidgets-dev
	ki18n-dev
	kiconthemes-dev
	knewstuff-dev
	knotifications-dev
	kpackage-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/frameworkintegration.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/frameworkintegration-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
dff114cd4b1a12156883d0490e8764a4df490d4a9959d7907eeaf20c8f2c5e14f92f0aa2f475213ed2471c4640a72d9b52c07b64df3429109b39ef5e9455ef14  frameworkintegration-5.109.0.tar.xz
"
