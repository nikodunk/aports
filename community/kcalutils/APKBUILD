# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kcalutils
pkgver=23.08.0
pkgrel=0
pkgdesc="The KDE calendar utility library"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://api.kde.org/kdepim/kcalutils/html"
license="LGPL-2.0-or-later"
depends_dev="
	grantlee-dev
	kcalendarcore-dev
	kcodecs-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	ki18n-dev
	kiconthemes-dev
	kidentitymanagement-dev
	kwidgetsaddons-dev
	"
makedepends="
	$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/kcalutils.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcalutils-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# kcalutils-testincidenceformatter and kcalutils-testtodotooltip are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kcalutils-test(incidenceformatter|todotooltip|dndfactory)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0398f5b964aba25af5c7649d0296dd20b3e6ce735865c58f22ec597771ab65fa45f31d960a947f2cee450f57c7797444e0592355e640f3867ea83973051b8845  kcalutils-23.08.0.tar.xz
"
