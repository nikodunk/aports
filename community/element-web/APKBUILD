# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: lauren n. liberda <lauren@selfisekai.rocks>
pkgname=element-web
pkgver=1.11.40
pkgrel=0
pkgdesc="A glossy Matrix collaboration client for the web"
url="https://element.io/"
#riscv64: ftbfs: memory access out of bounds
arch="noarch !riscv64"
options="!check"
license="Apache-2.0"
makedepends="
	nodejs
	yarn
"
source="
	https://github.com/vector-im/element-web/archive/refs/tags/v$pkgver/element-web-$pkgver.tar.gz
	no-source-maps.patch
	"
install="$pkgname.post-upgrade"
provides="riot-web=$pkgver-r$pkgrel"
replaces="riot-web"

export VERSION=$pkgver

# secfixes:
#   1.11.30-r0:
#     - CVE-2023-30609
#   1.11.26-r0:
#     - CVE-2023-28103
#     - CVE-2023-28427
#   1.11.7-r0:
#     - CVE-2022-39249
#     - CVE-2022-39250
#     - CVE-2022-39251
#     - CVE-2022-39236
#   1.11.4-r0:
#     - CVE-2022-36059
#     - CVE-2022-36060
#   1.9.7-r0:
#     - CVE-2021-44538
#   1.8.4-r0:
#     - CVE-2021-40823
#     - CVE-2021-40824

prepare() {
	default_prepare

	yarn install --frozen-lockfile --ignore-scripts
}

build() {
	NODE_ENV=production yarn build
}

package() {
	mkdir -p "$pkgdir"/usr/share/webapps \
		"$pkgdir"/etc/element-web
	cp -r webapp "$pkgdir"/usr/share/webapps/element-web
	ln -s ../element-web "$pkgdir"/usr/share/webapps/riot-web
	mv config.sample.json \
		"$pkgdir"/etc/element-web
	ln -sf /etc/element-web/config.json \
		"$pkgdir"/usr/share/webapps/element-web/config.json
}

sha512sums="
54795b8e4f6b09cbf6e381041c8f8c8de42a11451cd6df1e4689d2f63dce4c964fc830a8e4d0d2e4f0c5fd062c20c78d86a4f52a7a8339261a7a34da951b3723  element-web-1.11.40.tar.gz
92d964d9cd53b751da404bc75b4fbeee099414933a1aa644ff8d020ed2a48a4d856a92b0adffb1ee4606bddae6b498d6dc3db59f2c7292a22ee5d29ee6da18b9  no-source-maps.patch
"
