# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kbreakout
pkgver=23.08.0
pkgrel=0
pkgdesc="A Breakout-like game"
url="https://kde.org/applications/games/kbreakout/"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"
_repo_url="https://invent.kde.org/games/kbreakout.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kbreakout-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e3981005d043d1e17a43655f32b6ecc75711d68308a0c2a149b5d79f0e8a73ca36011603f25926498e660e4a8a667ec39efe7a9cbae74b9617fd1bcf18dba6ff  kbreakout-23.08.0.tar.xz
"
