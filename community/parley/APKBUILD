# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=parley
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/applications/education/org.kde.parley"
pkgdesc="Vocabulary Trainer"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	knotifications-dev
	kross-dev
	kxmlgui-dev
	libkeduvocdocument-dev
	libxml2-dev
	libxslt-dev
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	qt5-qtwebengine-dev
	samurai
	sonnet-dev
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/education/parley.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/parley-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1cf3bca1f961c38cbb174f8b1b0ba23cdd97e875dd52ecf8ce63f52d5e2c1be08171ffa0e82e2910d71c0cb04d3891c0861a2ed605262f88b042c778378d5d51  parley-23.08.0.tar.xz
"
