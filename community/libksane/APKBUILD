# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=libksane
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/graphics/"
pkgdesc="An image scanning library"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	ki18n-dev
	ksanecore-dev
	ktextwidgets-dev
	kwallet-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
_repo_url="https://invent.kde.org/graphics/libksane.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/libksane-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
50af59f2f22e2e1f35ec5fb1496b0aa6a39172175dda9f885ebb4f0170f76663dea224e433227071f7021ad267c310a4927b36dc97655558824bd373b48f07d2  libksane-23.08.0.tar.xz
"
