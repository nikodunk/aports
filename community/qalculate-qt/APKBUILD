# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
# version must be kept in sync with libqalculate
pkgname=qalculate-qt
pkgver=4.8.0
pkgrel=0
pkgdesc="Multi-purpose desktop calculator - Qt version"
url="https://github.com/Qalculate/qalculate-qt"
# qmake is broken on riscv64
arch="all !riscv64"
license="GPL-2.0-or-later"
depends="gnuplot"
makedepends="qt6-qtbase-dev qt6-qttools-dev libqalculate-dev"
subpackages="$pkgname-doc"
source="https://github.com/Qalculate/qalculate-qt/archive/v$pkgver/qalculate-qt-$pkgver.tar.gz"
options="!check" # no test suite

build() {
	export CFLAGS="$CFLAGS -flto=auto"
	export CXXFLAGS="$CXXFLAGS -flto=auto"
	qmake6 PREFIX=/usr
	make
}

check() {
	make check
}

package() {
	make INSTALL_ROOT="$pkgdir" install
}

sha512sums="
827d3802d486de70ce33a0c75f7992bcb1e37f4cd28048f29c13300b8b4f95c4ca459dd4fb3feac116cc3d90c5b7923fdee2c8b4a62f31adc90e1e64aa33bc10  qalculate-qt-4.8.0.tar.gz
"
