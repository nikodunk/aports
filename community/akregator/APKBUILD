# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=akregator
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://apps.kde.org/akregator/"
pkgdesc="RSS Feed Reader"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
makedepends="
	akonadi-mime-dev
	extra-cmake-modules
	grantlee-dev
	grantleetheme-dev
	kcmutils-dev
	kcrash-dev
	kdoctools-dev
	kiconthemes-dev
	knotifications-dev
	knotifyconfig-dev
	kontactinterface-dev
	kparts-dev
	kpimtextedit-dev
	ktexteditor-dev
	kxmlgui-dev
	libkdepim-dev
	libkleo-dev
	messagelib-dev
	pimcommon-dev
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	samurai
	syndication-dev
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/akregator.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/akregator-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
335d37957009f1e747f9f2aa58d3a0f44cbad488c852bd1298abf618ad2798902bcbd06808a34e6b51d16c182b7fd4b81d14cb990809e2b585a7b9b118d3a8ff  akregator-23.08.0.tar.xz
"
