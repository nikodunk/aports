# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kded
pkgver=5.109.0
pkgrel=0
pkgdesc="Extensible deamon for providing system level services"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
depends_dev="
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kservice-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kdoctools-dev
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/kded.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kded-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd
}

sha512sums="
bfa7917b5920e3879fd96f5f4c3d4c27c12de3f860c7dbd2dbbc9bc74c5c7dfce02a794d8ec04628ae7544c07c72acfd608fe7678f3f6da663792849981d0735  kded-5.109.0.tar.xz
"
