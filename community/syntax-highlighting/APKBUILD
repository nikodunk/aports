# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=syntax-highlighting
pkgver=5.109.0
pkgrel=0
pkgdesc="Syntax highlighting engine for structured text and code"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="MIT"
depends_dev="
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtxmlpatterns-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/syntax-highlighting.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/syntax-highlighting-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
bd8050dba60383d055cb9fd13948cd1b7117030ac63a67ce0e1bb0e474207f0fb594baa9b796f0e411e6e49bd2e4b9cd6caca3db6889485f5b491cf6703c352f  syntax-highlighting-5.109.0.tar.xz
"
