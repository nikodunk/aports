# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=oxygen-icons
pkgver=5.109.0
pkgrel=0
arch="noarch !armhf" # armhf blocked by extra-cmake-modules
pkgdesc="Oxygen icon theme"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
makedepends="$depends_dev
	extra-cmake-modules
	fdupes
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/oxygen-icons5.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/oxygen-icons5-$pkgver.tar.xz"
builddir="$srcdir/oxygen-icons5-$pkgver"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3b6ed954d8c7cfc45720b1eb0081e4f606f057261dbdd692b71068c525ba1de303a32b209a775f06d63b7f9b7567ab27f1354b177240430fb3a49bcaac7af695  oxygen-icons5-5.109.0.tar.xz
"
