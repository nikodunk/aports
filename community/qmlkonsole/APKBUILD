# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=qmlkonsole
pkgver=23.08.0
pkgrel=0
pkgdesc="Terminal app for Plasma Mobile"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/plasma-mobile/qmlkonsole"
license="GPL-3.0-or-later"
depends="
	qmltermwidget
	kirigami-addons
	kirigami2
	"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kirigami-addons-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	"
_repo_url="https://invent.kde.org/plasma-mobile/qmlkonsole.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/qmlkonsole-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
b01782a171723bfe0c52b0d1eece409cefd9331619d7b7d8f7eaa2bd889b51200ad51067f025ecb72dbe84197499f86bb1c971c7ca403fc3c95a300381e1f9c6  qmlkonsole-23.08.0.tar.xz
"
