# Contributor: Alex McGrath <amk@amk.ie>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-dataclasses-json
_pkgname=dataclasses-json
pkgver=0.6.0
pkgrel=0
pkgdesc="Provides a simple API for encoding and decoding dataclasses to and from JSON."
url="https://github.com/lidatong/dataclasses-json"
arch="all"
license="MIT"
depends="python3 py3-marshmallow py3-marshmallow-enum py3-typing_inspect py3-stringcase"
makedepends="py3-gpep517 py3-poetry-core py3-installer py3-poetry-dynamic-versioning"
checkdepends="py3-pytest py3-hypothesis py3-mypy"
subpackages="$pkgname-pyc"
source="dataclasses-json-$pkgver.tar.gz::https://github.com/lidatong/dataclasses-json/archive/v$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	export POETRY_DYNAMIC_VERSIONING_BYPASS="$pkgver"
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
38f10b0cd298fc8363482c9cb873ab140e08c03df2b7c9c4a40e41f56afd009c93f60fce06d27d0e593b5b88b94afe9f4da72fd009a82f136212fcc20a61a4b2  dataclasses-json-0.6.0.tar.gz
"
